<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!doctype html>
<html lang="en">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

        <title>Aggiungi genere</title>

        <!-- font awesome cdn link  -->
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">

                <!-- custom css file link  -->
                <link rel="stylesheet" href="../../css/style.css">

                <style>
                                .bottone{
                                        background-color: #4CAF50;
                                        border: none;
                                        color: white;
                                        padding: 10px 20px;
                                        text-align: center;
                                        text-decoration: none;
                                        display: inline-block;
                                        font-size: 18px;
                                        cursor: pointer;
                                        border-radius: 25px;
                                        text-transform:uppercase;
                                }
                                .bottone:hover{
                                background-color: lightblue;
                                transition: all .5s;
                                transform:scale(1.3);

                                }

                                </style>
    </head>

    <body>



    <div class="container">
        <div class="row g-5">
            <div class="col-md-7 col-lg-8">
                <h4 class="my-3">Aggiungi genere</h4>
                <form:form class="needs-validation" novalidate="" modelAttribute="genere_validator" action="/admin/gestione-catalogo/genere/aggiungi-genere" method="post">
                    <hr class="my-4">
                    <div class="row g-3">
                        <div class="col-5">
                            <label for="genere" class="form-label">Genere</label>
                            <div class="input-group has-validation">
                                <form:input type="text" class="form-control" id="genere" placeholder="Genere" path="nome_genere" />
                            </div>
                        </div>
                        <div class="mt-5">
<a href="#home" onclick="location.href='/homepage'" style="text-decoration:none;" class="bottone btn-outline-info">Home</a>
                            <button type="submit" class="bottone btn-outline-info">Salva</button>
                        </div>


                </form:form>
            </div>
        </div>

        <footer class="my-5 pt-5 text-muted text-center text-small">
            <p class="my-3 fixed-bottom">© 2021–2022 By Patrix</p>
        </footer>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>
    </body>

</html>

