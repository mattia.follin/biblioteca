<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Registrazione</title>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
        <link href="https://getbootstrap.com/docs/4.0/examples/signin/signin.css" rel="stylesheet" crossorigin="anonymous">
        <style>
                        .bottone{
                                background-color: #4CAF50;
                                border: none;
                                color: white;
                                padding: 8px 20px;
                                text-align: center;
                                text-decoration: none;
                                display: inline-block;
                                font-size: 20px;
                                cursor: pointer;
                                border-radius: 25px;
                                text-transform:uppercase;
                        }
                        .bottone:hover{
                        background-color: lightblue;
                        transition: all .5s;
                        transform:scale(1.1);

                        }

                        </style>
    </head>
    <body>
        <div class="container text-center" style="width:30%;">
            <form:form method="post" action="/register" modelAttribute="utente_register">
                <h2 class="form-signin-heading">Registrati</h2>
                <c:if test="${reg_error == true}">
                    Errore, dati inseriti non validi.
                </c:if>
                <p>
                    <label for="username" class="sr-only">Username</label>
                    <form:input type="text" id="username" name="username" class="form-control" placeholder="Username" path="username" />
                </p
                <p>
                    <label for="email" class="sr-only">Email</label>
                    <form:input type="email" id="email" name="email" class="form-control" placeholder="Email" path="email" />
                </p>
                <p>
                    <label for="password" class="sr-only">Password</label>
                    <form:input type="password" id="password" name="password" class="form-control" placeholder="Password" path="password" />
                </p>
                <p>
                    <label for="date" class="sr-only">Data di nascita</label>
                    <form:input type="date" id="date" name="date" class="form-control" placeholder="Data di nascita" path="dataDiNascita" />
                </p>
                <p>
                    <a href="/login" class="bottone" style="text-decoration:none; color:white;">Login</a>
                </p>
                <button class="bottone btn-lg btn-primary btn-block" type="submit">Registrati</button>
            </form:form>
        </div>
    </body>
</html>