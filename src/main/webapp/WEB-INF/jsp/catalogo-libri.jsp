<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!doctype html>
<html lang="en">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css"
              integrity="sha512-Fo3rlrZj/k7ujTnHg4CGR2D7kSs0v4LLanw2qksYuRlEzO+tcaEPQogQ0KaoGN26/zrn20ImR1DfuLWnOo7aBA=="
              crossorigin="anonymous" referrerpolicy="no-referrer" />

        <title>Catalogo Libri</title>

        <!-- font awesome cdn link  -->
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">

                <!-- custom css file link  -->
                <link rel="stylesheet" href="../../css/style.css">
    </head>
    <body>
    <!-- header section starts  -->

        <header class="header">

            <div class="header-1">

                        <a href="#" class="logo" style="text-decoration: none;"> <i class="fas fa-book"></i> Biblioteca </a>

                        <form action="" class="search-form">
                            <input type="search" name="" placeholder="Cerca qui..." id="search-box">
                            <label for="search-box" class="fas fa-search"></label>
                        </form>

                        <div class="icons">
                            <div id="search-btn" class="fas fa-search"></div>
                            <div id="login-btn" class="fas fa-user"></div>
                        </div>

            </div>

            <div class="header-2">
                <nav class="navbar justify-content-center">
                    <a href="#home" onclick="location.href='/homepage'" style="text-decoration:none;">Home</a>
                    <c:choose>
                        <c:when test="${admin == true}">
                            <a href="/catalogo-libri/show/true" style="text-decoration:none;">Catalogo</a>
                        </c:when>
                        <c:when test="${admin == null}">
                            <a href="/catalogo-libri/show/false" style="text-decoration:none;">Catalogo</a>
                        </c:when>
                    </c:choose>
                    <c:if test="${admin == true}">
                        <a href="/admin/gestione/utenti" style="text-decoration:none;">Gestione utenti</a>
                    </c:if>
                    <c:if test="${admin == true}">
                    <a href="/admin/gestione-catalogo/libro/form-aggiungi-libro" onclick="location.href='/admin/gestione-catalogo/libro/form-aggiungi-libro'" style="text-decoration:none;">Aggiungi libro</a>
                    <a href="/admin/gestione-catalogo/genere/lista-generi" onclick="location.href='/admin/gestione-catalogo/genere/lista-generi'" style="text-decoration:none;">Lista generi</a>
                    </c:if>
                    <a href="/logout" style="text-decoration:none;">Logout</a>
                </nav>
            </div>

        </header>

        <!-- header section ends -->

        <!-- bottom navbar  -->

        <nav class="bottom-navbar">
            <a href="#home" class="fas fa-home" onclick="location.href='/homepage'"></a>
            <c:choose>
                <c:when test="${admin == true}">
                    <a href="/catalogo-libri/show/true" class="fas fa-list"></a>
                </c:when>
                <c:when test="${admin == null}">
                    <a href="/catalogo-libri/show/false" class="fas fa-tags"></a>
                </c:when>
            </c:choose>
            <c:if test="${admin == true}">
                <a href="/admin/gestione/utenti" class="fas fa-comments"></a>
            </c:if>
        </nav>

        <!-- login form  -->

        <div class="login-form-container">

                <div id="close-login-btn" class="fas fa-times"></div>

                <form action="">
                    <h3>Profilo</h3>
                    <div style="text-align: center;>
                    <form action="/logout" method="post">
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"></input>
                        <a href="/logout" class="btn">Logout</a>
                    </form>
                    </div>

                </form>

        </div>


    <table class="table container exm">
        <thead>
        <tr>
            <c:if test="${admin==true}">
                <th scope="col">ID</th>
            </c:if>
            <th scope="col">Titolo</th>
            <th scope="col">Autore</th>
            <th scope="col">Genere</th>
            <th scope="col">Anno</th>
            <th scope="col">Stato</th>
            <c:if test="${admin == true}">
                <th scope="col">Immagine</th>
            </c:if>
        </tr>
        </thead>
        <tbody>
            <c:choose>
                <c:when test="${admin == true}">
                    <c:forEach var="libro" items="${catalogo_admin}">
                        <tr>
                            <th scope="row">${libro.id}</th>
                            <td>${libro.titolo}</td>
                            <td>${libro.autore}</td>
                            <td>${libro.genere}</td>
                            <td>${libro.anno}</td>
                            <td>${libro.stato}</td>
                            <td>${libro.immagine}</td>
                            <c:if test="${admin == true}">
                                <td><button type="button" class="bottone btn-primary" onclick="location.href='/admin/gestione-catalogo/libro/modifica-libro-form/${libro.id}'">Modifica</button></td>
                                <td><button type="button" class="bottone btn-danger" onclick="location.href='/admin/gestione-catalogo/libro/delete/${libro.id}'">Elimina</button></td>
                            </c:if>
                        </tr>
                    </c:forEach>
                </c:when>
                <c:when test="${admin == null}">
                    <c:choose>
                        <c:when test="${year_true == true}">
                            <c:forEach var="libro" items="${libri_filtrati}">
                                <tr>
                                    <td>${libro.titolo}</td>
                                    <td>${libro.autore}</td>
                                    <td>${libro.genere}</td>
                                    <td>${libro.anno}</td>
                                    <td>${libro.stato}</td>
                                    <c:if test="${admin == true}">
                                        <td><button type="button" class="btn btn-primary" onclick="location.href='/admin/gestione-catalogo/libro/modifica-libro-form/${libro.id}'">Modifica</button></td>
                                        <td><button type="button" class="btn btn-danger" onclick="location.href='/admin/gestione-catalogo/libro/delete/${libro.id}'">Elimina</button></td>
                                    </c:if>
                                </tr>
                            </c:forEach>
                        </c:when>
                        <c:otherwise>
                            <c:forEach var="libro" items="${catalogo}">
                                <tr>
                                    <td>${libro.titolo}</td>
                                    <td>${libro.autore}</td>
                                    <td>${libro.genere}</td>
                                    <td>${libro.anno}</td>
                                    <td>${libro.stato}</td>
                                    <c:if test="${admin == true}">
                                        <td><button type="button" class="btn btn-primary" onclick="location.href='/admin/gestione-catalogo/libro/modifica-libro-form/${libro.id}'">Modifica</button></td>
                                        <td><button type="button" class="btn btn-danger" onclick="location.href='/admin/gestione-catalogo/libro/delete/${libro.id}'">Elimina</button></td>
                                    </c:if>
                                </tr>
                            </c:forEach>
                        </c:otherwise>
                    </c:choose>
                </c:when>
            </c:choose>

        </tbody>
    </table>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>

    <!-- custom js file link  -->
        <script src="../../js/main.js"></script>
    </body>

</html>