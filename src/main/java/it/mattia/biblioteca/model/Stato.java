package it.mattia.biblioteca.model;

public enum Stato {

    DISPONIBILE, NON_DISPONIBILE

}
